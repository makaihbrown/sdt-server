const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const util = require('util');
const port = 8080;
const clients = [];

var Player = require('./Classes/Player.js');

console.log('Server has started');

var players = [];
var sockets = [];

io.on('connection', function(socket) {
    clients.push(socket.id);
    var clientConnectedMsg = 'Player connected ' + util.inspect(socket.id) + ', total: ' + clients.length;
    console.log(clientConnectedMsg);
    var player = new Player();
    var thisPlayerID = player.id;

    players[thisPlayerID] = player;
    sockets[thisPlayerID] = socket;

    socket.emit('register', {id: thisPlayerID}); //Register player with player ID to server
    socket.emit('spawn', player); //Spawn player
    socket.broadcast.emit('spawn', player) //Tell all player has spawned.

  for(var playerID in players) {
    if(playerID != thisPlayerID) {
      socket.emit('spawn', players[playerID]);
    }
  }

    socket.on('updatePosition', function(data) {
        player.position.x = data.position.x;
        player.position.y = data.position.y;l

        socket.broadcast.emit('updatePosition', player);
    });


    socket.on('disconnect', function() {
      clients.pop(socket.id);
      var clientDisconnectedMsg = 'Player disconnected ' + util.inspect(socket.id) + ', total: ' + clients.length;
      console.log(clientDisconnectedMsg);
      
      delete players[thisPlayerID];
      delete sockets[thisPlayerID];
      socket.broadcast.emit('disconnected', player);
    })

});

http.listen(port, function() {
  console.log('Listening on port *: ' + port);
});